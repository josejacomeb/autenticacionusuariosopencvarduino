#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import logging
from datetime import datetime
from signal import signal, SIGINT
from sys import exit

import cv2 #Libreria de OpenCV

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', level=logging.DEBUG)




def establecer_argumentos():
    """
        Funcion para inicializar los analizador de argumentos del programa
        :return: un objeto argparse
    """
    analizador_argumento = argparse.ArgumentParser()
    analizador_argumento.add_argument("--camara", '-camara', help="Identificador de la cámara o ruta al video",
                                      type=str, required=True)
    analizador_argumento.add_argument("--mostrar", '-m', help="Muestra o no las ventanas con la identificación "
                                                              "de las personas", type=bool, default=True)
    analizador_argumento.add_argument("--puerto_serial", '-ps', help="Direccion al puerto serial que se conecta a la PC,"
                                      " no especificar si es un arduino", type=str, default="arduino")
    analizador_argumento.add_argument("--tiempo_muestra", '-tm',
                                      help="Tiempo que se muestra la ventana luego de una deteccion",
                                      type=int, default=5)
    analizador_argumento.add_argument("--velocidad_serial", '-v',
                                      help="Velocidad de comunicación en el puerto serial con Arduino",
                                      type=int, default=115200)

    return analizador_argumento

def handler(signal_received, frame):
    logger.info("Control + C Presionado, el programa ha finalizado!")
    exit(0)

def main():
    '''
    Funcion principal de codigo
    :return: -1 si hay algun error
    '''
    import serial
    signal(SIGINT, handler)
    argumentos = establecer_argumentos().parse_args()
    dispositivo_video = argumentos.camara
    if dispositivo_video.isnumeric():
        dispositivo_video = int(dispositivo_video)

    captura = cv2.VideoCapture(dispositivo_video)
    ret, imagen = captura.read()
    diccionario_detecciones = {"d": "Desconocido", "c": "Conocido"}
    if ret is None:
        logger.warning("Por favor revise que el dispositivo {} este conectado o si su nombre esta bien escrito".format(dispositivo_video))
        return -1
    direccion_puerto_serial = argumentos.puerto_serial
    #Busca un arduino en los puertos seriales compatibles
    if direccion_puerto_serial == "arduino":
        import serial.tools.list_ports
        ports = list(serial.tools.list_ports.comports())
        if ports == []:
            logger.warning("No hay puertos disponibles")
        else:
            for port in ports:
                if "Arduino" in port.description:
                    direccion_puerto_serial = port.device #Guarda el puerto dle arduino

    if direccion_puerto_serial == "arduino": #Chequea de nuevo si hubo un cambio de puerto
        logger.error("Arduino no se pudo encontrar, por favor especifique manualmente el puerto del arduino con el "
                      "argumento --puerto_serial COM4 al ejecutar este programa")
        return -1
    objeto_serial = serial.Serial(direccion_puerto_serial, argumentos.velocidad_serial)  # Abre el puerto serial
    if not objeto_serial.isOpen():
        logger.error("No se pudo abrir el dispositivo con direccion serial {}, por favor "
                      "revise que este conectado o sea la ruta correcta".format(direccion_puerto_serial))
    else:
        logger.info("Computadora conectada con un dispositivo en el puerto serial {}".format(direccion_puerto_serial))
    mostrar_imagen = argumentos.mostrar
    fps = 30
    tiempo_frames_muestra = argumentos.tiempo_muestra * fps
    contador_frames_muestra = 0
    mensaje = {"mensaje":"", "color": (0,0,0), "posicion":(30,30), "fuente": cv2.FONT_HERSHEY_SIMPLEX,
               "escala_fuente": 1, "ancho_fuente": 2}
    mostrar_mensaje = False
    dato = ""
    logging.info("Programa iniciado!")
    while True:
        ret, imagen = captura.read()
        if ret is None:
            if type(dispositivo_video) == int:
                logger.warning("Cámara de video desconectada, no se puede continuar")
            else:
                logger.info("Video terminado, se termina el program")
            return -1

        if objeto_serial.in_waiting:
            dato = objeto_serial.readline().decode("utf-8").rstrip()
            if dato in diccionario_detecciones:
                mensaje["mensaje"] = "Usuario {} detectado!".format(diccionario_detecciones[dato])
                if dato == "c":
                    mensaje["color"] = (0, 255, 0)
                    mostrar_mensaje = True
                else:
                    mensaje["color"] = (0, 0, 255)
                    mostrar_mensaje = True
            else:
                logger.warning("Dato {} no puede ser reconocido como usuario".format(dato))

        if mostrar_mensaje:
            if contador_frames_muestra < tiempo_frames_muestra:
                if contador_frames_muestra == 0:
                    logger.info("Usuario {} ha accedido al sistema".format(diccionario_detecciones[dato]))
                    now = datetime.now() #Obtengo el tiempo actual
                    fecha_string = now.strftime("%d_%m_%Y-%H:%M:%S")
                    cv2.imwrite("{}_{}.png".format(diccionario_detecciones[dato], fecha_string), imagen)
                if mostrar_imagen:
                    image = cv2.putText(imagen, mensaje["mensaje"], mensaje["posicion"], mensaje["fuente"],
                                        mensaje["escala_fuente"], mensaje["color"], mensaje["ancho_fuente"], cv2.LINE_AA)
                    if mostrar_imagen:
                        cv2.imshow("Usuario", imagen)
                        caracter_pantalla = cv2.waitKey(30)
                        if caracter_pantalla == 27: #Presionar Escape para salir
                            break
                contador_frames_muestra += 1
            else:
                mostrar_mensaje = False
                contador_frames_muestra = 0
                if mostrar_imagen:
                    cv2.destroyWindow("Usuario")
    logger.info("Programa finalizado con exito!")
    objeto_serial.close() #Cierro objeto serial




if __name__ == '__main__':
    main()
